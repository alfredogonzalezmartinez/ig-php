# Mini aplicación estilo Instagram con PHP

Esta aplicación a sido desarrollada tomando como referencia el video "[CREANDO UN INSTAGRAM CON PHP 8, POO y MVC (Model View Controller)](https://youtu.be/4Nuyyoc2bPI)" de [Marcos Rivas](https://twitter.com/vidamrr).

**Indice**

- [Levantar entorno de desarrollo](#levantar-entorno-de-desarrollo)
- [Instalar dependencias de la aplicación](#instalar-dependencias-de-la-aplicación)
- [Inicializar la base de datos](#inicializar-la-base-de-datos)
- [Acceder al contenedor de la aplicación](#acceder-al-contenedor-de-la-aplicación)

## Levantar entorno de desarrollo

Para ejecutar la aplicación en modo de desarrollo ejecutaremos el siguiente comando:

```bash
  docker compose up -d
```

## Instalar dependencias de la aplicación

Para instalar dependencias de la aplicación ejecutaremos el siguiente comando:

```bash
  docker compose exec app composer install
```

## Inicializar la base de datos

Para añadir las tablas de la base de datos podemos ejecutar el siguiente comando.

**IMPORTANTE**: Solo debe ejecutarse la primera ver que se lance el entorno.

```bash
  docker compose exec -Ti db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE"' < db/schema.sql
```

## Acceder al contenedor de la aplicación

Para tener acceso a la linea de comandos del contenedor de la aplicación, en caso de necesitar instalar dependencias o ejecutar scripts con `composer`, ejecutaremos el siguiente comando:

```bash
  docker compose exec app sh
```
