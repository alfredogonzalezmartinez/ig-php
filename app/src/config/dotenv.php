<?php

$rootDir = dirname(dirname(__DIR__));
$envFile = $rootDir.'/.env';

$existsEnvFile = file_exists($envFile);

if ($existsEnvFile) {
    $dotenv = \Dotenv\Dotenv::createImmutable($rootDir);
    $dotenv->load();
}
