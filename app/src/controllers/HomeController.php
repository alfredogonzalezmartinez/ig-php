<?php

namespace Agm\Igphp\controllers;

use Agm\Igphp\lib\Controller;
use Agm\Igphp\lib\UtilImages;
use Agm\Igphp\models\PostImage;
use Agm\Igphp\models\User;

class HomeController extends Controller
{
    public function __construct(private User $user)
    {
        parent::__construct();
    }

    public function index()
    {
        $posts = PostImage::getFeed();

        $this->render('home/index', [
            'user' => $this->user,
            'posts' => $posts,
        ]);
    }

    public function publish()
    {
        $title = $this->post('title');
        $image = $this->file('image');

        if (is_null($title) || is_null($image)) {
            return header('location: /home');
        }

        try {
            $filename = UtilImages::storeImage($image);
        } catch (\Exception $e) {
            error_log($e->getMessage());

            return header('location: /home');
        }

        $post = new PostImage($title, $filename);
        $this->user->publish($post);

        header('location: /home');
    }
}
