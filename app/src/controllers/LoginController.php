<?php

namespace Agm\Igphp\controllers;

use Agm\Igphp\lib\Controller;
use Agm\Igphp\models\User;

class LoginController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->render('login/index');
    }

    public function auth()
    {
        $username = $this->post('username');
        $password = $this->post('password');

        if (is_null($username) || is_null($password)) {
            return header('location: /login');
        }

        $existsUser = User::exists($username);

        if (!$existsUser) {
            return header('location: /login');
        }

        $user = User::get($username);
        $isCorrectPassword = $user->comparePassword($password);

        if (!$isCorrectPassword) {
            return header('location: /login');
        }

        $_SESSION['user'] = serialize($user);

        return header('location: /home');
    }
}
