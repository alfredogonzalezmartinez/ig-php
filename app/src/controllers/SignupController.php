<?php

namespace Agm\Igphp\controllers;

use Agm\Igphp\lib\Controller;
use Agm\Igphp\lib\UtilImages;
use Agm\Igphp\models\User;

class SignupController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->render('signup/index');
    }

    public function register()
    {
        $username = $this->post('username');
        $password = $this->post('password');
        $profile = $this->file('profile');

        if (is_null($username) || is_null($password) || is_null($profile)) {
            return $this->render('error/index');
        }

        try {
            $picture = UtilImages::storeImage($profile);
        } catch (\Exception $e) {
            error_log($e->getMessage());

            return header('location: /login');
        }

        $user = new User($username, $password);
        $user->setProfile($picture);
        $isUserSaved = false !== $user->save();

        if (!$isUserSaved) {
            return $this->render('error/index');
        }

        header('location: /login');
    }
}
