<?php

namespace Agm\Igphp\lib;

class Database
{
    private string $host;
    private string $db;
    private string $user;
    private string $password;
    private string $charset;

    public function __construct()
    {
        $this->host = $_ENV['DB_HOST'];
        $this->db = $_ENV['DB_DATABASE'];
        $this->user = $_ENV['DB_USER'];
        $this->password = $_ENV['DB_PASSWORD'];
        $this->charset = $_ENV['DB_CHARSET'];
    }

    public function connect(): \PDO
    {
        $connection = 'mysql:host='.$this->host.';dbname='.$this->db.';charset='.$this->charset;
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            return new \PDO(
                $connection,
                $this->user,
                $this->password,
                $options
            );
        } catch (\PDOException $e) {
            throw $e;
        }
    }
}
