<?php

namespace Agm\Igphp\lib;

class Model
{
    private Database $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    protected function query($query)
    {
        return $this->db->connect()->query($query);
    }

    protected function prepare($query)
    {
        return $this->db->connect()->prepare($query);
    }
}
