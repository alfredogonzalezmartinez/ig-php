<?php

namespace Agm\Igphp\lib;

class UtilHashes
{
    public static function getHashedFilename(string $file)
    {
        $pathParts = pathinfo($file);
        $filename = $pathParts['filename'];
        $ext = $pathParts['extension'];

        return md5(date('Ymdgi').$filename).'.'.$ext;
    }

    public static function getHashedPassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]);
    }
}
