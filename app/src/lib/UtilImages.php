<?php

namespace Agm\Igphp\lib;

class UtilImages
{
    public static function storeImage(array $photo): string
    {
        $targetDir = 'img/photos/';
        $photoFile = $photo['name'];
        $photoTmpFile = $photo['tmp_name'];

        $hashedPhotoFile = UtilHashes::getHashedFilename($photoFile);

        $targetFile = $targetDir.$hashedPhotoFile;

        $check = getimagesize($photoTmpFile);
        $isUploadOk = false !== $check;

        if (!$isUploadOk) {
            throw new \Exception("The file {$photoFile} was not uploaded.");
        }

        $isMovedOk = move_uploaded_file($photoTmpFile, $targetFile);

        if (!$isMovedOk) {
            throw new \Exception("The file {$photoTmpFile} was not moved to {$targetFile}.");
        }

        return $hashedPhotoFile;
    }
}
