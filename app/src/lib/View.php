<?php

namespace Agm\Igphp\lib;

class View
{
    public $data;

    public function render(string $name, array $data = []): void
    {
        $this->data = $data;

        require dirname(__DIR__).'/views/'.$name.'.php';
    }
}
