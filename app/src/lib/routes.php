<?php

use Agm\Igphp\controllers\HomeController;
use Agm\Igphp\controllers\LoginController;
use Agm\Igphp\controllers\SignupController;

$router = new \Bramus\Router\Router();

session_start();

$user = isset($_SESSION['user'])
  ? unserialize($_SESSION['user'])
  : null;

$router->get('/', function () {
    echo phpinfo();
});

$router->get('/signup', function () {
    $controller = new SignupController();
    $controller->index();
});

$router->post('/register', function () {
    $controller = new SignupController();
    $controller->register();
});

$router->get('/login', function () {
    $controller = new LoginController();
    $controller->index();
});

$router->post('/auth', function () {
    $controller = new LoginController();
    $controller->auth();
});

$router->get('/home', function () {
    global $user;
    $controller = new HomeController($user);
    $controller->index();
});

$router->post('/publish', function () {
    global $user;
    $controller = new HomeController($user);
    $controller->publish();
});

$router->run();
