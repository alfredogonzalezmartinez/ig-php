<?php

namespace Agm\Igphp\models;

use Agm\Igphp\lib\Model;

class Like extends Model
{
    private int $id;

    public function __construct(private int $postId, private int $userId)
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getPostId()
    {
        return $this->postId;
    }

    public function save(): bool
    {
        try {
            $query = $this->prepare('INSERT INTO likes (post_id, user_id) VALUES(:post_id, :user_id)');

            $query->execute([
                'post_id' => $this->getPostId(),
                'user_id' => $this->getUserId(),
            ]);

            return true;
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return false;
        }
    }
}
