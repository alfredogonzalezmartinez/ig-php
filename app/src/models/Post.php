<?php

namespace Agm\Igphp\models;

use Agm\Igphp\lib\Model;

class Post extends Model
{
    private int $id;
    private array $likes;
    private User $user;

    protected function __construct(private string $title)
    {
        parent::__construct();
        $this->likes = [];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getLikes()
    {
        return count($this->likes);
    }

    public function fetchLikes()
    {
        $postId = $this->getId();
        $likes = [];

        try {
            $query = $this->prepare('SELECT * FROM likes WHERE post_id =:post_id');

            $query->execute(['post_id' => $postId]);

            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $likeId = $row['id'];
                $userId = $row['user_id'];

                $like = new Like($postId, $userId);
                $like->setId($likeId);

                $likes[] = $like;
            }

            $this->likes = $likes;
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    protected function addLike(User $user)
    {
        $postId = $this->getId();
        $userId = $user->getId();

        $like = new Like($postId, $userId);
        $like->save();

        $this->likes[] = $like;
    }
}
