<?php

namespace Agm\Igphp\models;

use Agm\Igphp\lib\Database;

class PostImage extends Post
{
    public function __construct(private string $title, private string $image)
    {
        parent::__construct($title);
    }

    public function getImage()
    {
        return $this->image;
    }

    public static function getFeed(): array
    {
        $posts = [];
        $db = new Database();

        try {
            $query = $db->connect()->prepare('SELECT * FROM posts ORDER BY post_id DESC');

            $query->execute();

            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $postId = $row['post_id'];
                $userId = $row['user_id'];
                $title = $row['title'];
                $image = $row['media'];

                $user = User::getById($userId);

                $post = new PostImage($title, $image);
                $post->setId($postId);
                $post->fetchLikes();
                $post->setUser($user);

                $posts[] = $post;
            }

            return $posts;
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return [];
        }
    }
}
