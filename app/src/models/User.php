<?php

namespace Agm\Igphp\models;

use Agm\Igphp\lib\Database;
use Agm\Igphp\lib\Model;
use Agm\Igphp\lib\UtilHashes;

class User extends Model
{
    private int $id;
    private array $posts;
    private string $profile;

    public function __construct(
        private string $username,
        private string $password
    ) {
        parent::__construct();
        $this->posts = [];
        $this->profile = '';
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setProfile(string $profile): void
    {
        $this->profile = $profile;
    }

    public function getProfile(): string
    {
        return $this->profile;
    }

    public function setPosts(array $posts): void
    {
        $this->posts = $posts;
    }

    public function getPosts(): array
    {
        return $this->posts;
    }

    public function save()
    {
        $existsUser = User::exists($this->username);

        if ($existsUser) {
            return false;
        }

        try {
            $hashedPassword = UtilHashes::getHashedPassword($this->password);

            $query = $this->prepare('INSERT INTO users (username, password, profile) VALUES(:username, :password, :profile)');

            $query->execute([
                'username' => $this->username,
                'password' => $hashedPassword,
                'profile' => $this->profile,
            ]);
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    public function comparePassword(string $password): bool
    {
        return password_verify($password, $this->password);
    }

    public function publish(PostImage $post): bool
    {
        try {
            $query = $this->prepare('INSERT INTO posts (user_id, title, media) VALUES(:user_id, :title, :media)');

            $query->execute([
                'user_id' => $this->getId(),
                'title' => $post->getTitle(),
                'media' => $post->getImage(),
            ]);

            return true;
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    public static function get(string $username): User
    {
        try {
            $db = new Database();

            $query = $db->connect()->prepare('SELECT * FROM users WHERE username=:username');

            $query->execute(['username' => $username]);

            $data = $query->fetch(\PDO::FETCH_ASSOC);

            $user = new User($data['username'], $data['password']);
            $user->setId($data['user_id']);
            $user->setProfile($data['profile']);

            return $user;
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return null;
        }
    }

    public static function getById(int $userId): User
    {
        try {
            $db = new Database();

            $query = $db->connect()->prepare('SELECT * FROM users WHERE user_id = :user_id');

            $query->execute(['user_id' => $userId]);

            $data = $query->fetch(\PDO::FETCH_ASSOC);

            $user = new User($data['username'], $data['password']);
            $user->setId($data['user_id']);
            $user->setProfile($data['profile']);

            return $user;
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return null;
        }
    }

    public static function exists(string $username)
    {
        try {
            $db = new Database();

            $query = $db->connect()->prepare('SELECT username FROM users WHERE username=:username');

            $query->execute(['username' => $username]);

            return 0 !== $query->rowCount();
        } catch (\PDOException $e) {
            error_log($e->getMessage());

            return true;
        }
    }
}
