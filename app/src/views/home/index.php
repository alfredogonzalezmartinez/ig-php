<?php
$user = $this->data['user'];
$posts = $this->data['posts'];
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Home</title>
</head>
<body>
  <p>¡Hola <?php echo $user->getUsername(); ?>!</p>

  <?php require_once '../src/components/createPostForm.php'; ?>

  <?php foreach ($posts as $post) {
      $owner = $post->getUser();
      $photosFolder = '/img/photos/';
      $ownerName = $owner->getUsername();
      $profileSrc = $photosFolder.$owner->getProfile();
      $profileAlt = 'Foto de perfil de '.$ownerName;
      $postSrc = $photosFolder.$post->getImage();
      $postAlt = 'Foto de '.$ownerName;
      ?>
    <article>
      <header>
        <img src="<?php echo $profileSrc; ?>" alt="<?php echo $profileAlt; ?>" width="30px">
        <span><?php echo $ownerName; ?></span>
      </header>
      <section>
        <img src="<?php echo $postSrc; ?>" alt="<?php echo $postAlt; ?>" width="300px">
        <p><?php echo $post->getTitle(); ?></p>
      </section>
      <footer>
        <form action="addLike" method="POST">
          <input type="hidden" name="post_id" value="<?php echo $post->getId(); ?>">
          <input type="hidden" name="user_id" value="<?php echo $user->getId(); ?>">
          <input type="hidden" name="origin" value="/home">
          <button type="submit"><?php echo $post->getLikes(); ?> Likes</button>
        </form>
      </footer>
    </article>
  <?php } ?>
</body>
</html>