<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Iniciar sesión</title>
</head>
<body>
  <form action="/auth" method="post">
    <label>
      Usuario
      <input type="text" name="username" />
    </label>
    <label>
      Contraseña
      <input type="password" name="password" />
    </label>
    <input type="submit" value="Iniciar sesión" />
  </form>
</body>
</html>