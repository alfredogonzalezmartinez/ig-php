<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Crear Cuenta</title>
</head>
<body>
  <form action="/register" method="post" enctype="multipart/form-data">
    <label>
      Usuario
      <input type="text" name="username">
    </label>
    <label>
      Contraseña
      <input type="password" name="password">
    </label>
    <label>
      Foto de perfil
      <input type="file" name="profile">
    </label>
    <input type="submit" value="Crear cuenta">
  </form>
</body>
</html>